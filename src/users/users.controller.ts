import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { InputUserDto } from './dtos/input-user.dto';
import { ReadUserDto } from './dtos/read-user.dto';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
  constructor(private readonly userService: UsersService) {}

  @Get()
  @UseGuards(AuthGuard('jwt'))
  async users(): Promise<ReadUserDto[]> {
    return await this.userService.users();
  }

  @Post()
  async createUser(@Body() userInput: InputUserDto): Promise<ReadUserDto> {
    return await this.userService.create(userInput);
  }

  @Post('login')
  @UseGuards(AuthGuard('local'))
  async login(@Body() userInput: InputUserDto): Promise<any> {
    return await this.userService.login(userInput);
  }
}
