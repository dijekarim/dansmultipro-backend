import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import { randomBytes, pbkdf2Sync } from 'crypto';
import { Model } from 'mongoose';
import { InputUserDto } from './dtos/input-user.dto';
import { ReadUserDto } from './dtos/read-user.dto';
import { User, UserDocument } from './schemas/user.schema';

@Injectable()
export class UsersService {
  private byte_size = 16;
  private iterations = 10000;
  private key_length = 64;
  constructor(
    @InjectModel(User.name) private userModel: Model<UserDocument>,
    private jwtService: JwtService,
  ) {}

  async users(): Promise<ReadUserDto[]> {
    return await this.userModel.find({});
  }

  async getUser(data: any): Promise<any> {
    return await this.userModel.findOne({ username: data });
  }

  async create(input: InputUserDto): Promise<ReadUserDto> {
    const { username, password } = input;
    const salt = this.makeSalt();
    const hashedPassword = this.encryptPassword(password, salt);
    console.log(salt, hashedPassword);
    const user = new this.userModel({
      username,
      password: hashedPassword,
      salt,
    });
    return user.save();
  }

  async login(input: InputUserDto): Promise<any> {
    const { username, password } = input;
    if (!username) {
      return await { error: 'Username harus ada!' };
    }
    const user = await this.userModel.findOne({ username: username });
    if (user.password !== this.encryptPassword(password, user.salt)) {
      return await { error: 'Password yang anda masukkan salah' };
    }
    const payload = {
      username: user.username,
      sub: user._id,
    };

    return await { access_token: this.jwtService.sign(payload) };
  }

  encryptPassword(password: string, salt: string): string {
    const salt_buffer = Buffer.from(salt, 'base64');
    const hashed = pbkdf2Sync(
      password,
      salt_buffer,
      this.iterations,
      this.key_length,
      'sha512',
    );
    return hashed.toString('hex');
  }

  makeSalt(): string {
    return randomBytes(this.byte_size).toString('base64');
  }
}
