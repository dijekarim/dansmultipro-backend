import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { CareersController } from './careers.controller';
import { CareersService } from './careers.service';

@Module({
  imports: [
    HttpModule,
    JwtModule.register({
      secret: 'secretKey',
      signOptions: { expiresIn: '60s' },
    }),
  ],
  controllers: [CareersController],
  providers: [CareersService, JwtService],
})
export class CareersModule {}
