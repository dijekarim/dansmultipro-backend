import { Controller, Get, Param, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CareersService } from './careers.service';

@Controller('careers')
export class CareersController {
  constructor(private readonly careersService: CareersService) {}

  @Get()
  @UseGuards(AuthGuard('jwt'))
  async all(@Query() query): Promise<any> {
    const { search, location, full_time } = query;
    return await this.careersService.findAll(search, location, full_time);
  }

  @Get(':id')
  @UseGuards(AuthGuard('jwt'))
  async detail(@Param() params): Promise<any> {
    const { id } = params;
    return await this.careersService.detail(id);
  }
}
