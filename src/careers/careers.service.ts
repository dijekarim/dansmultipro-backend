import { HttpService } from '@nestjs/axios';
import { ForbiddenException, Injectable } from '@nestjs/common';
import { catchError, map } from 'rxjs';

@Injectable()
export class CareersService {
  constructor(private readonly httpService: HttpService) {}

  async findAll(
    search: string,
    location: string,
    full_time: string,
  ): Promise<any> {
    const resp = this.httpService
      .get('http://dev3.dansmultipro.co.id/api/recruitment/positions.json', {
        params: {
          ...(search && { description: search }),
          ...(location && { location: location }),
          ...(full_time && { type: 'Full Time' }),
        },
      })
      .pipe(map((res) => res.data))
      .pipe(
        catchError(() => {
          throw new ForbiddenException('API not available');
        }),
      );

    return resp;
  }

  async detail(id: string): Promise<any> {
    const resp = this.httpService
      .get('http://dev3.dansmultipro.co.id/api/recruitment/positions.json')
      .pipe(
        map((res) => {
          return res.data?.find((data) => data.id === id);
        }),
      )
      .pipe(
        catchError(() => {
          throw new ForbiddenException('API not available');
        }),
      );

    return resp;
  }
}
